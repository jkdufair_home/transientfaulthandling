﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StudioKit.TransientFaultHandling.ErrorDetectionStrategies;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace StudioKit.TransientFaultHandling
{
	/// <summary>
	///     This class is a wrapper for HttpClient. It will attempt to make an HTTP request 3 times with a
	///     configurable timeout and a fixed, one second retry interval. It only wraps GetStringAsync right
	///     now, but it would be nice to have it properly wrap all the HttpClient functions.
	/// </summary>
	public class RetryingHttpClient
	{
		private readonly int _retryCount = 2;
		private readonly int _timeout = 5;

		public RetryingHttpClient()
		{
		}

		public RetryingHttpClient(int retryCount)
		{
			_retryCount = retryCount;
		}

		public RetryingHttpClient(int retryCount, int timeout)
		{
			_retryCount = retryCount;
			_timeout = timeout;
		}

		/// <summary>
		/// Send a GET HTTP request with retries.
		/// </summary>
		/// <param name="uri">The endpoint URI</param>
		/// <param name="credentials">(optional) Basic Auth credentials</param>
		/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
		/// <returns></returns>
		public async Task<string> GetStringAsync(Uri uri,
			string credentials = null,
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
		{
			string response = null;
			var policy =
				new RetryPolicy<HttpTransientErrorDetectionStrategy>(new FixedInterval(_retryCount,
					TimeSpan.FromSeconds(1)));

			await policy.ExecuteAsync(async () =>
			{
				var client = new HttpClient();
				if (credentials != null)
					client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);
				if (acceptHeaderValue != null)
					client.DefaultRequestHeaders.Accept.Add(acceptHeaderValue);
				client.Timeout = TimeSpan.FromSeconds(_timeout);
				try
				{
					response = await client.GetStringAsync(uri).ConfigureAwait(false);
				}
				catch (OperationCanceledException)
				{
					throw new HttpRequestException("Request timed out");
				}
			}).ConfigureAwait(false);
			return response;
		}

		/// <summary>
		/// Send a POST HTTP request with retries. Must use a Func to generate the HttpContent per request/retry, otherwise it is Disposed between retries.
		/// </summary>
		/// <param name="uri">The endpoint URI</param>
		/// <param name="getHttpContent">A function to generate the request content.</param>
		/// <param name="credentials">(optional) Basic Auth credentials</param>
		/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
		/// <returns></returns>
		public async Task<string> PostAsync(Uri uri,
			Func<Uri, HttpContent> getHttpContent,
			string credentials = null,
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
		{
			string responseBody = null;
			var policy =
				new RetryPolicy<HttpTransientErrorDetectionStrategy>(new FixedInterval(_retryCount,
					TimeSpan.FromSeconds(1)));

			await policy.ExecuteAsync(async () =>
			{
				var client = new HttpClient();
				if (credentials != null)
					client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);
				if (acceptHeaderValue != null)
					client.DefaultRequestHeaders.Accept.Add(acceptHeaderValue);
				client.Timeout = TimeSpan.FromSeconds(_timeout);
				HttpResponseMessage response;
				var content = getHttpContent(uri);
				try
				{
					response = await client.PostAsync(uri, content).ConfigureAwait(false);
				}
				catch (OperationCanceledException)
				{
					throw new HttpRequestException("Request timed out");
				}
				response.EnsureSuccessStatusCode();
				responseBody = await response.Content.ReadAsStringAsync();
			}).ConfigureAwait(false);
			return responseBody;
		}

		/// <summary>
		/// Send a HTTP request message with retries.
		/// </summary>
		/// <param name="httpRequestMessage">The HttpRequestMessage to be sent</param>
		/// <param name="credentials">(optional) Basic Auth credentials</param>
		/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
		/// <returns></returns>
		public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage, string credentials = null,
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
		{
			HttpResponseMessage response = null;
			var policy =
				new RetryPolicy<HttpTransientErrorDetectionStrategy>(new FixedInterval(_retryCount,
					TimeSpan.FromSeconds(1)));

			await policy.ExecuteAsync(async () =>
			{
				var client = new HttpClient();
				if (credentials != null)
					client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);
				if (acceptHeaderValue != null)
					client.DefaultRequestHeaders.Accept.Add(acceptHeaderValue);
				client.Timeout = TimeSpan.FromSeconds(_timeout);
				try
				{
					response = await client.SendAsync(httpRequestMessage).ConfigureAwait(false);
				}
				catch (OperationCanceledException)
				{
					throw new HttpRequestException("Request timed out");
				}
				response.EnsureSuccessStatusCode();
			}).ConfigureAwait(false);
			return response;
		}
	}
}