﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;

namespace StudioKit.TransientFaultHandling.Extensions
{
	public static class RetryPolicyExtensions
	{
		/// <summary>
		/// Attach a logging function to the retry policy's "Retrying" EventHandler.
		/// </summary>
		/// <param name="retryPolicy"></param>
		/// <param name="logFunction"></param>
		public static void SetupRetryingEventHandler(this RetryPolicy retryPolicy, Action<Exception> logFunction)
		{
			// Receive notifications about retries.
			retryPolicy.Retrying += (sender, args) =>
			{
				if (logFunction != null)
					logFunction.Invoke(new Exception($"Retry - Count: {args.CurrentRetryCount}, Delay: {args.Delay}",
										args.LastException));
			};
		}
	}
}