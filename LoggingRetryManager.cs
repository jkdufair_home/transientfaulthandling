﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StudioKit.TransientFaultHandling.Extensions;
using System;
using System.Collections.Generic;

namespace StudioKit.TransientFaultHandling
{
	public class LoggingRetryManager : RetryManager
	{
		public Action<Exception> LogFunction;

		public LoggingRetryManager(IEnumerable<RetryStrategy> retryStrategies) : base(retryStrategies)
		{
		}

		public LoggingRetryManager(IEnumerable<RetryStrategy> retryStrategies, string defaultRetryStrategyName)
				: base(retryStrategies, defaultRetryStrategyName)
		{
		}

		public LoggingRetryManager(IEnumerable<RetryStrategy> retryStrategies, string defaultRetryStrategyName, IDictionary<string, string> defaultRetryStrategyNamesMap)
				: base(retryStrategies, defaultRetryStrategyName, defaultRetryStrategyNamesMap)
		{
		}

		public override RetryPolicy<T> GetRetryPolicy<T>()
		{
			var retryPolicy = base.GetRetryPolicy<T>();
			if (LogFunction != null)
				retryPolicy.SetupRetryingEventHandler(LogFunction);
			return retryPolicy;
		}

		public override RetryPolicy<T> GetRetryPolicy<T>(string retryStrategyName)
		{
			var retryPolicy = base.GetRetryPolicy<T>(retryStrategyName);
			if (LogFunction != null)
				retryPolicy.SetupRetryingEventHandler(LogFunction);
			return retryPolicy;
		}
	}
}