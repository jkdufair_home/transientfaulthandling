﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;

namespace StudioKit.TransientFaultHandling.RetryStrategies
{
	public class TransientErrorCatchAllStrategy : ITransientErrorDetectionStrategy
	{
		/// <summary>
		/// Always returns true.
		/// </summary>
		/// <param name="ex">The exception.</param>
		/// <returns>Always true.</returns>
		public bool IsTransient(Exception ex)
		{
			return true;
		}
	}
}